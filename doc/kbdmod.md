This yaml file conventionally resides in `/etc/interception/kbdmod`.

You can use raw (integer) keycodes, however it is easier to use the `#define`d strings from [input-event-codes.h](https://github.com/torvalds/linux/blob/master/include/uapi/linux/input-event-codes.h).

Note that this program is intercepting the key events before they get translated to any keyboard layout. This means that the keynames only refer to the key position on the US keyboard.
![us keyboard](https://www.uk-yankee.com/sites/default/files/800px-kb_united_states-noaltgr_svg.png)

each key can do four different actions depending on how you press them
 - tap
 - hold
 - doubletap
 - taphold(doubletaping but not releasing the key on the second press)

each of these mappings can do three different things
 - [normal output](###output)
 - [osm](###osm)
 - [program internal command](###command)

in addition you can have different layers which in essence allow you to change the modifications to the keys. The program will always start on the first layer listed in the config

### output
output one or multiple keycodes

### osm
output one or multiple keycodes when another key is pressed
for example shift osm and then keycode a -> shift press keycode a shift release

### command
#### switch to layer
>switch_layer: *layername to switch to*

#### switch to layer or toggle layer
when holding the key while pressing other keys the layer it will switch back when the key is released
>switch_toggle_layer: *layername to switch to*

#### switch to layer for only one keypress or toggle by holding
>switch_osm_layer: *layername to switch to*

#### autoshift 
autoshift adds a shift to keys with no hold mapping and only a single output on the tap key when they are pressed and held the content after : is eralevant use 0 or smth like that.
>autoshift_on: 0

>autoshift_off: 0

>autoshift_toggle: 0

#### custom_input_events
custom_input_events lets you output custom linux input_events press and release must be less than 10 events
```yaml
custom_input_events:
  press:
    - type: event_type
      code: event_code
      value: event_value
    ...
  release:
    - type: e_type
	...
```
    

### running multiple commands
```yaml
multiple_commands:
  some command: args for the command
  another command:
  ...
```

to output events with while running commands use multiple commands with the standard_output command the arguments work the same as with normal output keys
>standard_output: [key1,key2]



```yaml
# unmapped keys will behave like they do without this program
# max amount of ms to qualify as a tap default = 200
hold_time: <integer>
	
# add numbers togethere to get desired behaviour
# enable from start on 1
# autoshift every digit 2
# every letter 4
# every special character 8
# for example 3(2+1) for digits and at start enabled
autoshift: <integer>

layers:
	#name of the layer
	somelayer:
		#required to create mapping the string option is a keyname from https://github.com/torvalds/linux/blob/master/include/uapi/linux/input-event-codes.h the integer option would be the number defined in there
		#NOTE keycodes above 248 are not handled.
    	- key: <integer | string>
    	  tap: [ <integer | string>, ... ]
    	  hold: [ <integer | string>, ... ]
		  doubletap: [ <integer | string>, ... ]
		  taphold: [ <integer | string>, ... ]
		  tap_osm: bool
		  doubletap_osm: bool
		  hold_osm: bool
		  taphold_osm: bool
        - key: <integer | string>
          tap:
            <string>: command
    	- key: ...
	otherlayer:
		- key: ...
	...
```
