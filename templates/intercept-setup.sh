#!/bin/bash

#cd into script dir
cd "$(dirname "$0")"

#read kbdmod config file
read -p "Enter the config file name you will use for kbdmod: " config_file

#Get keyboard name
keyboard="$(libinput list-devices | grep Device | grep eyboard)"
#remove "Device: " from string
keyboard="${keyboard//Device: /}"

echo "using $keyboard as input device to intercept"

#copy config file to /etc/inteception/udevmon.d/
sudo cp udevmon.yaml /etc/interception/udevmon.d/

#replace keyboard name in config file
sudo sed -i "s/keyboard_name/$keyboard/g" /etc/interception/udevmon.d/udevmon.yaml
#replace config file name in config file
sudo sed -i "s/config_file_name/$config_file/g" /etc/interception/udevmon.d/udevmon.yaml