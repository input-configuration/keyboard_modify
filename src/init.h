#pragma once
#include <string.h>
#include <sstream>
#include <libevdev-1.0/libevdev/libevdev.h>
#include "key.h"
#include "mapping.h"
#include "timer.h"
#include "autoshift.h"
#include "command.h"
#include "sll.h"
#include <yaml-cpp/yaml.h>
#include <fstream>
#include <iostream>
#include <map>
using std::string;
using std::exception;
using std::stringstream;

void init(string configPath);
//this function is copied from
//https://gitlab.com/interception/linux/plugins/dual-function-keys
//convert event code to string
inline int event_code(const string code) {

    int ret = libevdev_event_code_from_name(EV_KEY, code.c_str());
    if (ret == -1)
        ret = strtol(code.c_str(), NULL, 10);

    // KEY_RESERVED is invalid
    if (ret == 0)
        fprintf(stderr, "%s is an invalid key code\n", code.c_str());

    return ret;
}

inline int event_type(const string type) {

    int ret = libevdev_event_type_from_name(type.c_str());
    if (ret == -1)
        ret = strtol(type.c_str(), NULL, 10);

    // KEY_RESERVED is invalid
    if (ret == 0)
        fprintf(stderr, "%s is an invalid key type\n", type.c_str());

    return ret;
}

inline int event_value(const string value, const int type, const int code) {

    int ret = libevdev_event_value_from_name(type, code, value.c_str());
    if (ret == -1)
        ret = strtol(value.c_str(), NULL, 10);

    // KEY_RESERVED is invalid
    if (ret == 0)
        fprintf(stderr, "%s is an invalid key value\n", value.c_str());

    return ret;
}
