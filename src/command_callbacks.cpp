#include "command_callbacks.h"

class Layer;

extern Layer* Layers;
extern Layer* AktiveLayer;
extern Layer* PreviousLayer;

bool LayerToggle = false;

//callback in case commands are dependend on intel on other keys
void(*command_callback)(input_event*) = null_command_callback;
void(*next_callback)(input_event*)  = null_command_callback;

//null callback to avoid branching
void null_command_callback(input_event*) {}

//change after one call to avoid calling it after the command release on the same key
void one_time_switch_callback(input_event* input) {
    command_callback = next_callback;
}

void osm_layer_callback(input_event* input) {
    auto tmp = AktiveLayer;
    AktiveLayer = PreviousLayer;
    PreviousLayer = tmp;
    command_callback = null_command_callback;
}

void layer_toggle_callback(input_event* input) {
    if (input->value == INPUT_VAL_PRESS) {
        LayerToggle = true;
        command_callback = null_command_callback;
        return;
    }
}