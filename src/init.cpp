#include "init.h"
#include "key.h"
#include "types.h"

#ifndef VERSION
#define VERSION "version undefined"
#endif

const int timing__tap_millisec = 200;

extern milliseconds delay;
extern Layer* AktiveLayer;
extern Layer* Layers;
extern Layer* PreviousLayer;
extern uint8_t LayerCount;
extern uint8_t autoShift;
extern OutputStorage SHIFT_OUTPUT;

Layer load_layer(const YAML::Node &layerconf, std::map<string, int> &layernames);

//init
void init(string configPath) {
    //read yaml file
    YAML::Node config;

	sllInit();
	KeyCode shiftBuff[2] = {1, KEY_LEFTSHIFT};
	SHIFT_OUTPUT = OutputStorage(shiftBuff, 2);

    try {
        config = YAML::LoadFile(configPath);
    } catch (const exception &e) {
        fprintf(stderr, "cannot read '%s': %s\n", configPath, e.what());
        exit(EXIT_FAILURE);
    }

	//load timing
	if (config["hold_time"]) {
		delay = milliseconds(config["hold_time"].as<uint>());
	}else{
		delay = milliseconds(timing__tap_millisec);
	}
	//load features
	if (config["autoshift"]) {
		autoShift = config["autoshift"].as<uint>();
	}
    //load keymap
    const YAML::Node& layers = config["layers"];

	if(layers.IsMap()){
		std::map<string, int> layerMap;
		int i = 0;
		for(const auto &it : layers){
			layerMap[it.first.as<string>()] = i;
			i++;
		}
		LayerCount = i;

		Layers = new Layer[LayerCount];
		for (auto &it : layerMap) {
			Layers[it.second] = load_layer(layers[it.first], layerMap);
		}
	}

	if (autoShift & 1)
		enable_autoshift();

	AktiveLayer = Layers;
	PreviousLayer = Layers;
}

size_t push_key_to_buffer(uint8_t* buffer, YAML::Node config) {
	if (config.Type() == YAML::NodeType::Sequence) {
		buffer[0] = config.size();
		for (const auto &it : config) {
			buffer++;
			buffer[0] = event_code(it.as<string>());
		}
		if (config.size() > 255) {
			fprintf(stderr, "push_key_to_buffer: sequence on key %s is too long\n", config.as<string>().c_str());
			buffer[0] = 0;
			return 1;
		}
		return config.size() + 1;
	}else{
		buffer[0] = 1;
		buffer[1] = event_code(config.as<string>());
		return 2;
	}
}

void load_mouse_ev(uint16_t code, uint8_t* buffer) {
	buffer[1] = Command::custom_input_events;
	buffer[0] = 3 + 4*sizeof(input_event)/sizeof(KeyCode);
	buffer[2] = 2*sizeof(input_event)/sizeof(KeyCode);
	buffer+=3;
	((input_event*)buffer)[0] = input_event{
		.type = EV_MSC,
		.code = MSC_SCAN,
		.value = MOUSE_SCAN_VALUE
	};
	buffer+=sizeof(input_event)/sizeof(KeyCode);
	((input_event*)buffer)[0] = input_event{
		.type = EV_KEY,
		.code = code,
		.value = INPUT_VAL_PRESS
	};
	buffer+=sizeof(input_event)/sizeof(KeyCode);
	buffer[0] = 2*sizeof(input_event)/sizeof(KeyCode);
	buffer+=1;
	((input_event*)buffer)[0] = input_event{
		.type = EV_MSC,
		.code = MSC_SCAN,
		.value = MOUSE_SCAN_VALUE
	};
	buffer+=sizeof(input_event)/sizeof(KeyCode);
	((input_event*)buffer)[0] = input_event{
		.type = EV_KEY,
		.code = code,
		.value = INPUT_VAL_RELEASE
	};
}

size_t load_cmd_to_buffer(
	const YAML::detail::iterator_value &cmd,
	std::map<string, int> &layernames,
	uint8_t* buffer
) {
	if(cmd.first.as<string>() == "switch_layer"){
		buffer[1] = Command::switch_layer;
		buffer[2] = layernames[cmd.second.as<string>()];
		buffer[0] = 2;
	}else if(cmd.first.as<string>() == "switch_toggle_layer") {
		buffer[1] = Command::switch_toggle_layer;
		buffer[2] = layernames[cmd.second.as<string>()];
		buffer[0] = 2;
	}else if(cmd.first.as<string>() == "switch_osm_layer") {
		buffer[1] = Command::switch_osm_layer;
		buffer[2] = layernames[cmd.second.as<string>()];
		buffer[0] = 2;
	}else if(cmd.first.as<string>() == "autoshift_on") {
		buffer[1] = Command::autoshift_on;
		buffer[0] = 1;
	}else if(cmd.first.as<string>() == "autoshift_off") {
		buffer[1] = Command::autoshift_off;
		buffer[0] = 1;
	}else if(cmd.first.as<string>() == "autoshift_toggle") {
		buffer[1] = Command::autoshift_toggle;
		buffer[0] = 1;
	}else if(cmd.first.as<string>() == "multiple_commands") {
		//check if mapping is a map
		if (cmd.second.IsMap()) {
			buffer[1] = Command::multiple_commands;
			size_t size = 2;
			for (const auto &it : cmd.second)
				size += load_cmd_to_buffer(it, layernames, buffer + size);
			buffer[0] = size;
		}else{
			fprintf(stderr, "multiple_commands: MAPPING is not a map\n to avoid this error, add cmd called null and use a map\n");
			buffer[1] = Command::null;
			buffer[0] = 1;
		}
	}else if(cmd.first.as<string>() == "standard_output") {
		buffer[1] = Command::standard_output;
		buffer[0] = push_key_to_buffer(buffer + 2, cmd.second) + 2;
	}else if(cmd.first.as<string>() == "custom_input_events") {
		auto original_buffer = buffer;
		if (!cmd.second.IsMap()) {
			fprintf(stderr, "custom_input_events: MAPPING is not a map nulling key");
			buffer[1] = Command::null;
			buffer[0] = 1;
		}else if(cmd.second["press"].size() + cmd.second["release"].size() + 3 > 0xFF) {
			fprintf(stderr, "custom_input_events: too many events only 10 events in total allowed. nulling key\n");
			buffer[1] = Command::null;
			buffer[0] = 1;
		}else{
			buffer[1] = Command::custom_input_events;
			uint8_t size = 3 + (cmd.second["press"].size() + cmd.second["release"].size())*sizeof(input_event)/sizeof(KeyCode);
			buffer[0] = size;
			buffer[2] = cmd.second["press"].size()*sizeof(input_event)/sizeof(KeyCode);
			buffer += 3;
			for (const auto &it : cmd.second["press"]) {
				((input_event*)buffer)[0] = input_event{
					.type = it["type"].as<uint16_t>(),
					.code = it["code"].as<uint16_t>(),
					.value = it["value"].as<int>()
				};
				buffer+=sizeof(input_event)/sizeof(KeyCode);
			}
			buffer[0] = cmd.second["release"].size()*sizeof(input_event)/sizeof(KeyCode);
			buffer += 1;
			for (const auto &it : cmd.second["release"]) {
				((input_event*)buffer)[0] = input_event{
					.type = it["type"].as<uint16_t>(),
					.code = it["code"].as<uint16_t>(),
					.value = it["value"].as<int>()
				};
				buffer+=sizeof(input_event)/sizeof(KeyCode);
			}
		}
		buffer = original_buffer;
	}else if(cmd.first.as<string>() == "mouse_left_click") {
		load_mouse_ev(BTN_LEFT, buffer);
	}else if(cmd.first.as<string>() == "mouse_right_click") {
		load_mouse_ev(BTN_RIGHT, buffer);
	}else if(cmd.first.as<string>() == "mouse_middle_click") {
		load_mouse_ev(BTN_MIDDLE, buffer);
	}else if(cmd.first.as<string>() == "null") {
		buffer[1] = Command::null;
		buffer[0] = 1;
	}else{
		fprintf(stderr, "unknown command: %s. nulling key \n", cmd.first.as<string>().c_str());
		buffer[1] = Command::null;
		buffer[0] = 1;
	}
	if (buffer[0] > 255) {
		fprintf(stderr, "buffer overflow. nulling key%s\n", cmd.first.as<string>().c_str());
		buffer[1] = Command::null;
		buffer[0] = 1;
	}
	return buffer[0]+1;
}

Layer load_layer(
const YAML::Node &layerconf, std::map<string, int> &layernames)
{
	int max = 0;
	int min = std::numeric_limits<int>::max();
	KeyCode buffer[1028];//max size used per key
	uint16_t kfbm;

	for(const auto &it : layerconf) {
		if (it["key"]) {
			min = std::min(min, event_code(it["key"].as<string>()));
			max = std::max(max, event_code(it["key"].as<string>()));
		}
	}
	if (autoShift & AUTOSHIFT_DIGIT_MASK) {
		min = std::min(min, KEY_1);
		max = std::max(max, KEY_0);
	}
	if (autoShift & AUTOSHIFT_LETTER_MASK) {
		min = std::min(min, KEY_Q);
		max = std::max(max, KEY_M);
	}
	if (autoShift & AUTOSHIFT_SPECIAL_CHARACTER_MASK) {
		min = std::min(min, KEY_MINUS);
		max = std::max(max ,KEY_102ND);
	}
	if(min > max) {
		fprintf(stderr, "No valid key found in config.\n");
		exit(EXIT_FAILURE);
	}

	Layer layer(min, max);

	for (const auto &it : layerconf) {
		//reset key feature bit mask and buffer begin
		size_t len = 0;
		kfbm = 0;

		//check if unvalid option is set
		for(const auto &it2 : it) {
			if(it2.first.as<string>() != "key" &&
			   it2.first.as<string>() != "tap" &&
			   it2.first.as<string>() != "hold" &&
			   it2.first.as<string>() != "doubletap" &&
			   it2.first.as<string>() != "taphold" &&
			   it2.first.as<string>() != "tap_osm" &&
			   it2.first.as<string>() != "hold_osm" &&
			   it2.first.as<string>() != "doubletap_osm" &&
			   it2.first.as<string>() != "taphold_osm"
			) {
				fprintf(stderr, "warning option: %s. there may be unexpected behaviour\n", it2.first.as<string>().c_str());
			}
		}

		if (it["tap"]) {
			if(it["tap"].IsMap()) {
				kfbm |= ON_TAP_COMMAND_MASK;
				len += load_cmd_to_buffer(*it["tap"].begin(), layernames, buffer);
			}else{
				len += push_key_to_buffer(buffer, it["tap"]);
				if (it["tap_osm"])
					kfbm |= ON_TAP_OSM_MASK;
			}
		}else{
			fprintf(stderr, "No tap feature found for key %s\n", it["key"].as<string>().c_str());
		}
		if (it["hold"]) {
			kfbm |= HOLD_ENABLED_MASK;
			if(it["hold"].IsMap()) {
				kfbm |= ON_HOLD_COMMAND_MASK;
				len += load_cmd_to_buffer(*it["hold"].begin(), layernames, buffer + len);
			}else{
				len += push_key_to_buffer(buffer+len, it["hold"]);
				if (it["hold_osm"])
					kfbm |= ON_HOLD_OSM_MASK;
			}
		}else{
			buffer[len] = 0;
			len++;
		}
		if (it["doubletap"]) {
			kfbm |= DOUBLETAP_ENABLED_MASK;
			if(it["doubletap"].IsMap()) {
				kfbm |= ON_DOUBLETAP_COMMAND_MASK;
				len += load_cmd_to_buffer(*it["doubletap"].begin(), layernames, buffer + len);
			}else{
				len += push_key_to_buffer(buffer+len, it["doubletap"]);
				if (it["doubletap_osm"])
					kfbm |= ON_DOUBLETAP_OSM_MASK;
			}
		}else{
			buffer[len] = 0;
			len++;
		}
		if(it["taphold"]) {
			kfbm |= TAPHOLD_ENABLED_MASK;
			if(it["taphold"].IsMap()) {
				kfbm |= ON_TAPHOLD_COMMAND_MASK;
				len += load_cmd_to_buffer(*it["taphold"].begin(), layernames, buffer + len);
			}else{
				len += push_key_to_buffer(buffer+len, it["taphold"]);
				if (it["taphold_osm"])
					kfbm |= ON_TAPHOLD_OSM_MASK;
			}
		}

		if (!(kfbm &
		HOLD_ENABLED_MASK +
		DOUBLETAP_ENABLED_MASK +
		TAPHOLD_ENABLED_MASK))
			len -= 2;
		//add mapping to layer
        layer[event_code(it["key"].as<string>())].init(
            kfbm, OutputStorage(buffer, len));
	}

	for(int i = 0; i < layer.size(); i++){
		if(layer[i].get_output().is_empty()) {
			buffer[0] = 1;
			buffer[1] = i;
			layer[i].init(0, OutputStorage(buffer, 2));
		}
	}

	for(auto &i : layer){
		if(i.get_output().size() < 2)
			autoshift_init(*i.get_output().begin(), &i.key);
	}

	return layer;
}
