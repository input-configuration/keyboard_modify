#pragma once
#include "mapping.h"
#include "sll.h"

class Layer;

inline Layer* Layers;
inline Layer* AktiveLayer;
inline Layer* PreviousLayer;
inline uint8_t LayerCount;

class Layer{
public:
	inline Layer(uint min, uint max)
	: min(min), max(max), mappings((new mapping[max - min + 1])-min){}
	inline void destruct(){delete[](mappings + min);}
	inline Layer(){}
	inline mapping* begin(){return mappings + min;}
	inline mapping* end(){return mappings + max +1;}
	inline mapping &operator[](uint8_t key){return mappings[key];}
	inline uint8_t size(){return max - min + 1;}
	inline bool contains(uint8_t key){return key >= min && key <= max;}
	inline void execute_press(uint8_t key) {
		if(!contains(key)){
			mapping tmp;
			uint8_t buffer[2] = {1, key};
			tmp.init(0, OutputStorage(buffer, 2));
			tmp.press();
		}else{
			mappings[key].press();
		}
		unreleased.push(key);
	}
	inline bool try_release(uint8_t key) {
		if(unreleased.pop(key)){
			if(!contains(key)){
				mapping tmp;
				uint8_t buffer[2] = {1, key};
				tmp.init(TAP_OUTPUT_PRESSED_MASK, OutputStorage(buffer, 2));
				tmp.release();
			}else{
				mappings[key].release();
			}
			return true;
		}
		return false;
	}
private:
	sll unreleased;
	mapping* mappings;
	uint8_t min;
	uint8_t max;
};
