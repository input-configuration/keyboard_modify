#pragma once
#include <stdint.h>

//singly linked list

struct sllElement{
    uint8_t next;
    uint8_t key;
};

const uint8_t sllBufferSize = 128;
inline sllElement sllMemory[sllBufferSize];//no realocation for faster speed
const auto sllEnd = sllBufferSize + sllMemory;
inline uint8_t sllAllocHead = 0;

inline void sllInit(){
    for (uint8_t i = 0; i < sllBufferSize; i++) sllMemory[i].next = i + 1;
}

//not thread safe should only be accesed by input_loop
class sll {
private:
    uint8_t head;
public:
    inline void push(uint8_t key) {
        if (sllAllocHead == sllBufferSize) {
            return;
        }
        uint8_t current = sllAllocHead;
        sllAllocHead = sllMemory[sllAllocHead].next;
        sllMemory[current] = {head, key};
        head = current;
    }
    inline bool pop(uint8_t key){
        sllElement* prev = nullptr;
        for (auto it = head + sllMemory; it != sllEnd; it = it->next + sllMemory) {
            if (it->key == key && prev != nullptr) {
                prev->next = it->next;
                it->next = sllAllocHead;
                sllAllocHead = it-sllMemory;
                return true;
            }else if(it->key == key){
                head = it->next;
                it->next = sllAllocHead;
                sllAllocHead = it-sllMemory;
                return true;
            }
            prev = it;
        }
        return false;
    }
    inline sll():head(sllBufferSize){}
};
