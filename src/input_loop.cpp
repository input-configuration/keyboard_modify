#include "input_loop.h"
#include <linux/input.h>
#include "io.h"
#include "layer.h"
#include <mutex>


extern void(*command_callback)(input_event*);
extern Layer* PreviousLayer;
extern Layer* AktiveLayer;
extern Layer* Layers;
extern uint8_t LayerCount;
extern mapping* AktiveKey;
extern IOTYPE IO;
bool running = true;
const uint8_t LAST_KEY = 248;
std::mutex layer_mutex;

inline void handle_input(input_event input) {
    layer_mutex.lock();

    static uint8_t AktiveKeyCode;
    switch (input.value) {
	case INPUT_VAL_PRESS:
        if(AktiveKey != nullptr && AktiveKeyCode != input.code)
	    	AktiveKey->consume_event();
        
        AktiveKeyCode = input.code;
        AktiveLayer->execute_press(input.code);
        break; 
    case INPUT_VAL_RELEASE:
        if(AktiveLayer->try_release(input.code))
            break;
        if(PreviousLayer->try_release(input.code))
            break;
        for(Layer* layer = Layers; layer < Layers + LayerCount; layer++)
            if(layer != AktiveLayer && layer != PreviousLayer)
                if(layer->try_release(input.code)){
                    command_callback(&input);
                    layer_mutex.unlock();
                    return;
                }
        fprintf(stderr, "release not handled by any layer. BUG: please contact the developer");
    case INPUT_VAL_REPEAT://handled by DE
        break;
    default:		
        fprintf(stderr, "unexpected .value=%d .code=%d, doing nothing",
        input.value,
        input.code);
    }
    //for commands requiring intel on other keys
    command_callback(&input);
    layer_mutex.unlock();
}

void input_loop() {
    input_event input;

	setbuf(stdout, NULL);
	setbuf(stdin, NULL);

    while (IO.read_event(&input))
    {
		if (input.type == EV_MSC && input.code == MSC_SCAN)
            continue;

		//pass non keyboard and unsuported keys throught to the system
        if (input.type != EV_KEY) {
            IO.write_event(&input);
			continue;
		}

/*pass through codes which are not defined by the linux/input.h to avoid undefined behaviour
ignore keys greater than 255 as well since they dont fit in a uint8_t and are not relevant
for a keyboard*/
        if (input.code > LAST_KEY) {
            IO.write_event(&input);
            continue;
        }

        handle_input(input);
	}
    running = false;
}
