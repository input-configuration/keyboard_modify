#pragma once
#include "types.h"
#include "mapping.h"
#include "layer.h"
#include "autoshift.h"

enum Command : uint8_t {
    null,
    switch_layer,
    switch_toggle_layer,
    switch_osm_layer,
    autoshift_on,
    autoshift_off,
    autoshift_toggle,
    multiple_commands,
    standard_output,
    custom_input_events,
};

//press of command
void command_press(OutputStorage command);
//release of command
void command_release(OutputStorage command);
