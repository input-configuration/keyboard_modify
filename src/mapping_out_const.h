#pragma once
#include "io.h"
#include "command.h"

extern IOTYPE IO;
extern mapping* AktiveKey;

//call .h to avoid autodetection by cmake

template<output_type type>
inline void mapping::write_output_release() {
    reset();
    if constexpr (type == tapT) {
        if (!(key & ON_TAP_OSM_MASK + ON_TAP_COMMAND_MASK))
			IO.write_event_release(tap());
        else if(key & ON_TAP_COMMAND_MASK)
            command_release(tap());
    }else if constexpr (type == doubletapT){
        if (!((key & ON_DOUBLETAP_OSM_MASK + ON_DOUBLETAP_COMMAND_MASK))) 
			IO.write_event_release(doubletap());
        else if(key & ON_DOUBLETAP_COMMAND_MASK)
			command_release(doubletap());
    }else if constexpr (type == holdT){
        if ((key & (ON_HOLD_OSM_MASK + ON_HOLD_COMMAND_MASK)) == 0) 
			IO.write_event_release(hold());
        else if(key & ON_HOLD_COMMAND_MASK)
			command_release(hold());
    }else if constexpr (type == tapholdT){
        if (!(key & ON_TAPHOLD_OSM_MASK + ON_TAPHOLD_COMMAND_MASK)) 
			IO.write_event_release(taphold());
		else if(key & ON_HOLD_COMMAND_MASK)
			command_release(taphold());
    }
}

//write output event
template<output_type type>
inline void mapping::write_output_press() {
    AktiveKey = nullptr;
    if constexpr (type == tapT)
        if (!(key & ON_TAP_OSM_MASK + ON_TAP_COMMAND_MASK)) {
			IO.write_event_press(tap());
			key |= TAP_OUTPUT_PRESSED_MASK;
        } else if(key & ON_TAP_OSM_MASK){
            IO.set_osm(tap());
        } else {
			command_press(tap());
			key |= TAP_OUTPUT_PRESSED_MASK;
		}
    else if constexpr (type == doubletapT)
        if (!((key & ON_DOUBLETAP_OSM_MASK + ON_DOUBLETAP_COMMAND_MASK))) {
			IO.write_event_press(doubletap());
			key |= DOUBLETAP_OUTPUT_PRESSED_MASK;
        } else if(key & ON_DOUBLETAP_OSM_MASK) {
            IO.set_osm(doubletap());
        } else {
			command_press(doubletap());
			key |= DOUBLETAP_OUTPUT_PRESSED_MASK;
		}
    else if constexpr (type == holdT)
        if (!(key & ON_HOLD_OSM_MASK + ON_HOLD_COMMAND_MASK)) {
			IO.write_event_press(hold());
			key |= HOLD_OUTPUT_PRESSED_MASK;
        } else if(key & ON_HOLD_OSM_MASK) {
            IO.set_osm(hold());
        } else {
			command_press(hold());
			key |= HOLD_OUTPUT_PRESSED_MASK;
		}
    else if constexpr (type == tapholdT)
        if (!(key & ON_TAPHOLD_OSM_MASK + ON_TAPHOLD_COMMAND_MASK)) {
			IO.write_event_press(taphold());
			key |= TAPHOLD_OUTPUT_PRESSED_MASK;
		} else if(key & ON_HOLD_OSM_MASK) {
            IO.set_osm(taphold());
        } else {
			command_press(taphold());
			key |= TAPHOLD_OUTPUT_PRESSED_MASK;
		}
}

template<output_type type>
inline void mapping::write_output() {
    write_output_press<type>();
    write_output_release<type>();
}
