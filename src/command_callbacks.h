#pragma once
#include <linux/input.h>
#include <stdint.h>
#include "io.h"

void null_command_callback(input_event*);
void osm_layer_callback(input_event*);
void layer_toggle_callback(input_event*);
void one_time_switch_callback(input_event*);