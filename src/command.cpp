#include "command.h"
#include "command_callbacks.h"

class Layer;

extern Layer* Layers;
extern Layer* AktiveLayer;
extern Layer* PreviousLayer;
extern IOTYPE IO;

extern bool LayerToggle;
extern void(*command_callback)(input_event*);
extern void(*next_callback)(input_event*);

void command_press(OutputStorage command) {
	command_callback = null_command_callback;
	switch (command[0]) {
	case Command::switch_osm_layer:
	case Command::switch_toggle_layer:
		LayerToggle = false;
		command_callback = one_time_switch_callback;
		next_callback = layer_toggle_callback;
	case Command::switch_layer:
		PreviousLayer = AktiveLayer;
		AktiveLayer = Layers+command[1];	
		break;
	case Command::autoshift_on:
		enable_autoshift();
		break;
	case Command::autoshift_off:
		disable_autoshift();
		break;
	case Command::autoshift_toggle:
		toggle_autoshift();
		break;
	case Command::multiple_commands:
		for (auto i = command.substorage(1); i != command.end(); i = i.next())
			command_press(i);
		break;
	case Command::standard_output:
		IO.write_event_press(command.substorage(1));
		break;
	case Command::custom_input_events:
		for (auto i = command.substorage(1).begin();
		i != command.substorage(1).end();
		i+=sizeof(input_event)/sizeof(KeyCode))
			IO.write_event((input_event*)(i));
		break;
	case Command::null:
		break;
	default:
		fprintf(stderr, "Unknown command: %d\n", command[0]);
		break;
	}
}

void command_release(OutputStorage command) {
	switch (command[0]) {
	case Command::switch_osm_layer:
		if (!LayerToggle){
			command_callback = one_time_switch_callback;
			next_callback = osm_layer_callback;
		}else{
			AktiveLayer = PreviousLayer;
			LayerToggle = false;
		}
		break;
	case Command::switch_toggle_layer:
		if (!LayerToggle){
			command_callback = null_command_callback;
		}else{
			AktiveLayer = PreviousLayer;
			LayerToggle = false;
		}
		break;
	case Command::multiple_commands:
		for (auto i = command.substorage(1); i != command.end(); i = i.next())
			command_release(i);
		break;
	case Command::standard_output:
		IO.write_event_release(command.substorage(1));
		break;
	case Command::custom_input_events:
		for (auto i = command.substorage(1).next().begin();
		i != command.end();
		i+=sizeof(input_event)/sizeof(KeyCode)) {
			IO.write_event((input_event*)(i));
		}
		break;
	}
}
