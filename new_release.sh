#! /bin/bash

# script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

cd kbdmod

# get current version
CURRENT_VERSION=$(grep pkgver PKGBUILD | cut -d "=" -f 2)
#get current release
CURRENT_RELEASE=$(grep pkgrel PKGBUILD | cut -d "=" -f 2)

cd ../

# add 1 to the version
case $1 in
    "major")
        NEW_VERSION=$(echo $CURRENT_VERSION | awk -F. '{$1=$1+1; print $1".0.0"}')
        ;;
    "minor")
        NEW_VERSION=$(echo $CURRENT_VERSION | awk -F. '{$2=$2+1; print $1"."$2".0"}')
        ;;
    "patch")
        NEW_VERSION=$(echo $CURRENT_VERSION | awk -F. '{$3=$3+1; print $1"."$2"."$3}')
        ;;
	"set-version")
		NEW_VERSION=$2
		CURRENT_RELEASE=$(($3-1))
		;;
    *)
        echo "Invalid argument"
        exit 1
        ;;
esac
# add 1 to the release
NEW_RELEASE=$(($CURRENT_RELEASE + 1))

# update version in install.sh
sed -i "s/VERSION=.*/VERSION=$NEW_VERSION/" install.sh

# create new git tag
# ask for confirmation
read -p "Create new git tag v$NEW_VERSION? [y/n] " -n 1 -r answer
if([[ $answer =~ ^[Yy]$ ]])
then
    git add install.sh
    git commit -m "new release: $NEW_VERSION-$NEW_RELEASE"
    git push
	#add new git tag
	git tag -a "v$NEW_VERSION" -m "new release: $NEW_VERSION-$NEW_RELEASE"
	git push --tags
fi

cd kbdmod
echo "Updated PKGBUILD to $NEW_VERSION-$NEW_RELEASE"

git pull

#remove old checksums
sed -i '/^sha256sums=/d' PKGBUILD

# update version and release
sed -i "s/pkgver=.*/pkgver=$NEW_VERSION/" PKGBUILD
sed -i "s/pkgrel=.*/pkgrel=$NEW_RELEASE/" PKGBUILD

# update checksum
makepkg -g >> PKGBUILD
# update srcinfo
makepkg --printsrcinfo > .SRCINFO

git add PKGBUILD .SRCINFO
git commit -m "v$NEW_VERSION-$NEW_RELEASE"

# push to aur
# ask for confirmation
read -p "Push to AUR? [y/n] " -n 1 -r answer
if([[ $answer =~ ^[Yy]$ ]])
then
    git push aur master
fi

rm "kbdmod-v$NEW_VERSION.tar.gz"

echo "Done"
