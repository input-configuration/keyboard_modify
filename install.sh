#! /bin/bash

# loop parameters
for i in "$@"; do
	case $i in
		-h|--help)
			echo "Usage: install.sh [OPTION]..."
			echo "Install the project"
			echo "  -h, --help              display this help and exit"
			echo "  --DESTDIR=DIR           install to DIR"
			exit 0
			;;
		--DESTDIR=*)
			DESTDIR="${i#*=}"
			shift
			;;
		--buildtype=*)
			buildtype="${i#*=}"
			shift
			;;
	esac
done

# check if DESTDIR is set
if [ -z $DESTDIR ]; then
	DESTDIR="/usr/bin"
fi

# check if buildtype is set
if [ -z $buildtype ]; then
	buildtype="Release"
fi

# get package version
VERSION=2.1.4

# build
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=$buildtype -Dversion="$VERSION"
make -j 8
cd ../

# so I can copy paste to pkgbuild
pkgdir=""

install -Dm 755 build/kbdmod.bin $DESTDIR/kbdmod
echo "kbdmod installed to $DESTDIR/kbdmod"

install -Dm 600 templates/mouse_props.yaml "$pkgdir/etc/interception/properties/mouse.yaml"

install -Dm 644 LICENSE "$pkgdir/usr/share/licenses/kbdmod/LICENSE"
echo "LICENSE installed to /usr/share/licenses/kbdmod/LICENSE"

install -Dm 644 README.md "$pkdir/usr/share/doc/kbdmod/README.md"
echo "README.md installed to /usr/share/doc/kbdmod/README.md"

install -Dm 644 doc/* "$pkgdir/usr/share/doc/kbdmod/"
echo "doc installed to /usr/share/doc/kbdmod/"

install -Dm 644 examples/laptop.yaml "$pkgdir/usr/share/doc/kbdmod/examples/laptop.yaml"
echo "laptop.yaml installed to /usr/share/doc/kbdmod/examples/laptop.yaml"

#check if cowsay is installed
if [ -x "$(command -v cowsay)" ]; then
	cowsay "ya installed kbdmod successfully"
else
	echo '_________________________________
<ya installed kbmod successfully> 
---------------------------------
        \   ^__^ 
         \  (oo)\_______ 
            (__)\       )\/\ 
                ||----w | 
                ||     ||'
fi

